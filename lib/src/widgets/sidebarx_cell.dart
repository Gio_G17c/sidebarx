import 'package:flutter/material.dart';
import 'package:sidebarx/sidebarx.dart';

class SidebarXCell extends StatefulWidget {
  const SidebarXCell({
    Key? key,
    required this.item,
    required this.extended,
    required this.selected,
    required this.theme,
    required this.onTap,
    required this.onLongPress,
    required this.onSecondaryTap,
    required this.animationController,
  }) : super(key: key);

  final bool extended;
  final bool selected;
  final SidebarXItem item;
  final SidebarXTheme theme;
  final VoidCallback onTap;
  final VoidCallback onLongPress;
  final VoidCallback onSecondaryTap;
  final AnimationController animationController;

  @override
  State<SidebarXCell> createState() => _SidebarXCellState();
}

class _SidebarXCellState extends State<SidebarXCell> {
  late Animation<double> _animation;
  var _hovered = false;

  @override
  void initState() {
    super.initState();
    _animation = CurvedAnimation(
      parent: widget.animationController,
      curve: Curves.easeIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = widget.theme;
    final iconTheme = widget.selected
        ? theme.selectedIconTheme
        : _hovered
            ? theme.hoverIconTheme ?? theme.selectedIconTheme
            : theme.iconTheme;
    final textStyle = widget.selected
        ? theme.selectedTextStyle
        : _hovered
            ? theme.hoverTextStyle
            : theme.textStyle;
    final decoration = (widget.selected
        ? widget.extended
            ? theme.selectedItemDecoration
            : theme.selectedItemDecorationCollapsed
        : theme.itemDecoration);
    final margin =
        (widget.selected ? theme.selectedItemMargin : theme.itemMargin);
    final padding = (widget.extended
        ? (widget.selected ? theme.selectedItemPadding : theme.itemPadding)
        : (widget.selected
            ? theme.selectedItemPaddingCollapsed
            : theme.itemPaddingCollapsed));
    final textPadding =
        widget.selected ? theme.selectedItemTextPadding : theme.itemTextPadding;
// Inside the build method of _SidebarXCellState class
    return MouseRegion(
      onEnter: (_) => _onEnteredCellZone(),
      onExit: (_) => _onExitCellZone(),
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: widget.onTap,
        onLongPress: widget.onLongPress,
        onSecondaryTap: widget.onSecondaryTap,
        behavior: HitTestBehavior.opaque,
        child: Row(
          mainAxisAlignment: widget.extended
              ? MainAxisAlignment.start
              : MainAxisAlignment.start,
          children: [
            Padding(
              padding: widget.extended
                  ? const EdgeInsets.only(right: 14)
                  : widget.selected
                      ? EdgeInsets.fromLTRB(10,0, 6,0)
                      : EdgeInsets.only(right: 12),
              child: widget.selected
                  ? Container(
                      width: 7,
                      height: 15,
                      decoration: const ShapeDecoration(
                        color: Color(0xFF477EFF),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(16),
                            bottomRight: Radius.circular(16),
                          ),
                        ),
                      ),
                    )
                  : const SizedBox.shrink(),
            ),
            Expanded(
              child: Container(
                decoration: decoration?.copyWith(
                  color: widget.extended
                      ? ((_hovered) ? theme.hoverColor : null)
                      : null,
                ),
                padding: widget.extended
                    ? padding ?? const EdgeInsets.all(8)
                    : widget.selected
                        ? const EdgeInsets.fromLTRB(8, 8, 0, 8)
                        : const EdgeInsets.fromLTRB(19, 8, 0, 8),
                margin: widget.extended
                    ? margin ?? const EdgeInsets.all(4)
                    : const EdgeInsets.only(right: 22, bottom: 3.5),
                child: Row(
                  children: [
                    AnimatedBuilder(
                      animation: _animation,
                      builder: (context, _) {
                        final value = ((1 - _animation.value) * 6).toInt();
                        if (value <= 0) {
                          return const SizedBox();
                        }
                        return Spacer(flex: value);
                      },
                    ),
                    if (widget.item.iconBuilder != null)
                      widget.item.iconBuilder!.call(widget.selected, _hovered)
                    else if (widget.item.icon != null)
                      _Icon(item: widget.item, iconTheme: iconTheme)
                    else if (widget.item.iconWidget != null)
                      widget.item.iconWidget!,
                    widget.extended
                        ? Container(
                            width: 256,
                            child: FadeTransition(
                              opacity: _animation,
                              child: Padding(
                                padding: textPadding ?? EdgeInsets.zero,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      widget.item.label ?? '',
                                      style: textStyle,
                                      overflow: TextOverflow.fade,
                                      maxLines: 1,
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_rounded,
                                      color: widget.selected
                                          ? Color(0xFF212121)
                                          : Color(0xFF666666),
                                      size: 20,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: SizedBox.shrink(),
                          ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onEnteredCellZone() {
    setState(() => _hovered = true);
  }

  void _onExitCellZone() {
    setState(() => _hovered = false);
  }
}

class _Icon extends StatelessWidget {
  const _Icon({
    Key? key,
    required this.item,
    required this.iconTheme,
  }) : super(key: key);

  final SidebarXItem item;
  final IconThemeData? iconTheme;

  @override
  Widget build(BuildContext context) {
    return Icon(
      item.icon,
      color: iconTheme?.color,
      size: iconTheme?.size,
    );
  }
}
