import 'package:flutter/material.dart';

class SidebarXTheme {
  const SidebarXTheme({
    this.width = 70,
    this.height = double.infinity,
    this.padding = EdgeInsets.zero,
    this.margin = EdgeInsets.zero,
    this.decoration,
    this.iconTheme,
    this.selectedIconTheme,
    this.textStyle,
    this.selectedTextStyle,
    this.itemDecoration,
    this.selectedItemDecoration,
    this.selectedItemDecorationCollapsed,
    this.itemMargin,
    this.selectedItemMargin,
    this.itemPadding,
    this.itemPaddingCollapsed,
    this.selectedItemPadding,
    this.selectedItemPaddingCollapsed,
    this.itemTextPadding,
    this.selectedItemTextPadding,
    this.hoverColor,
    this.hoverTextStyle,
    this.hoverIconTheme,
  });

  /// [SidebarX] width
  final double width;

  /// [SidebarX] height
  final double height;

  /// [SidebarX] padding
  final EdgeInsets padding;

  /// [SidebarX] margin
  final EdgeInsets margin;

  /// [SidebarX] decoration
  final BoxDecoration? decoration;

  /// Unselected [SidebarXItem] icons theme
  final IconThemeData? iconTheme;

  /// Selected [SidebarXItem] icon theme
  final IconThemeData? selectedIconTheme;

  /// Hovered [SidebarXItem] icon theme
  final IconThemeData? hoverIconTheme;

  /// Unselected [SidebarXItem] text style
  final TextStyle? textStyle;

  /// Selected [SidebarXItem] text  style
  final TextStyle? selectedTextStyle;

  /// Unselected item [SidebarXItem] decoration
  final BoxDecoration? itemDecoration;

  /// Selected item [SidebarXItem] decoration
  final BoxDecoration? selectedItemDecoration;

  /// Selected item [SidebarXItem] decoration
  final BoxDecoration? selectedItemDecorationCollapsed;

  /// Unselected item [SidebarXItem] marging
  final EdgeInsets? itemMargin;

  /// Selected item [SidebarXItem] marging
  final EdgeInsets? selectedItemMargin;

  /// Unselected item [SidebarXItem] padding
  final EdgeInsets? itemPadding;

  /// Unselected item [SidebarXItem] padding when collpsed
  final EdgeInsets? itemPaddingCollapsed;

  /// Selected item [SidebarXItem] padding
  final EdgeInsets? selectedItemPadding;

  /// Selected item [SidebarXItem] padding when collpsed
  final EdgeInsets? selectedItemPaddingCollapsed;

  /// Unselected item [SidebarXItem] text padding
  final EdgeInsets? itemTextPadding;

  /// Selected item [SidebarXItem] text padding
  final EdgeInsets? selectedItemTextPadding;

  /// Background color of [SidebarXItem] when the mouse
  /// cursor hover over an item
  final Color? hoverColor;

  /// [SidebarXItem] text  style  when the mouse
  /// cursor hover over an item
  final TextStyle? hoverTextStyle;

  /// Method to get default flutter theme settings
  SidebarXTheme mergeFlutterTheme(BuildContext context) {
    final theme = Theme.of(context);
    final mergedTheme = SidebarXTheme(
      width: width,
      height: height,
      padding: padding,
      margin: margin,
      decoration: decoration ?? BoxDecoration(color: theme.cardColor),
      iconTheme: iconTheme ?? theme.iconTheme,
      selectedIconTheme: selectedIconTheme ??
          theme.iconTheme.copyWith(color: theme.primaryColor),
      hoverIconTheme: selectedIconTheme ??
          theme.iconTheme.copyWith(color: theme.primaryColor),
      textStyle: textStyle ?? theme.textTheme.bodyMedium,
      selectedTextStyle: selectedTextStyle ??
          theme.textTheme.bodyMedium?.copyWith(color: theme.primaryColor),
      itemDecoration: itemDecoration,
      selectedItemDecoration: selectedItemDecoration,
      selectedItemDecorationCollapsed: selectedItemDecorationCollapsed,
      itemMargin: itemMargin,
      selectedItemMargin: selectedItemMargin,
      itemPadding: itemPadding,
      itemPaddingCollapsed: itemPaddingCollapsed,
      selectedItemPadding: selectedItemPadding,
      selectedItemPaddingCollapsed: selectedItemPaddingCollapsed,
      itemTextPadding: itemTextPadding,
      selectedItemTextPadding: selectedItemTextPadding,
      hoverColor: hoverColor ?? theme.hoverColor,
      hoverTextStyle: hoverTextStyle ??
          theme.textTheme.bodyMedium?.copyWith(color: theme.primaryColor),
    );
    return mergedTheme;
  }

  /// Merges two themes together
  SidebarXTheme mergeWith(
    SidebarXTheme theme,
  ) {
    return SidebarXTheme(
      width: width,
      height: height,
      padding: padding,
      margin: margin,
      itemTextPadding: itemTextPadding ?? theme.itemTextPadding,
      selectedItemTextPadding:
          selectedItemTextPadding ?? theme.selectedItemTextPadding,
      decoration: decoration ?? theme.decoration,
      iconTheme: iconTheme ?? theme.iconTheme,
      selectedIconTheme: selectedIconTheme ?? theme.selectedIconTheme,
      textStyle: textStyle ?? theme.textStyle,
      selectedTextStyle: selectedTextStyle ?? theme.selectedTextStyle,
      itemMargin: itemMargin ?? theme.itemMargin,
      selectedItemMargin: selectedItemMargin ?? theme.selectedItemMargin,
      itemPadding: itemPadding ?? theme.itemPadding,
      itemPaddingCollapsed: itemPaddingCollapsed ?? theme.itemPaddingCollapsed,
      selectedItemPadding: selectedItemPadding ?? theme.selectedItemPadding,
      selectedItemPaddingCollapsed:
          selectedItemPaddingCollapsed ?? selectedItemPaddingCollapsed,
      itemDecoration: itemDecoration ?? theme.itemDecoration,
      selectedItemDecoration:
          selectedItemDecoration ?? theme.selectedItemDecoration,
      hoverColor: hoverColor ?? theme.hoverColor,
      hoverTextStyle: hoverTextStyle ?? theme.hoverTextStyle,
      hoverIconTheme: hoverIconTheme ?? theme.hoverIconTheme,
    );
  }

  /// Defautl copyWith method
  SidebarXTheme copyWith({
    double? width,
    double? height,
    EdgeInsets? padding,
    EdgeInsets? margin,
    BoxDecoration? decoration,
    IconThemeData? iconTheme,
    IconThemeData? selectedIconTheme,
    TextStyle? textStyle,
    TextStyle? selectedTextStyle,
    BoxDecoration? itemDecoration,
    BoxDecoration? selectedItemDecoration,
    EdgeInsets? itemMargin,
    EdgeInsets? selectedItemMargin,
    EdgeInsets? itemPadding,
    EdgeInsets? itemPaddingCollapsed,
    EdgeInsets? selectedItemPadding,
    EdgeInsets? selectedItemPaddingCollapsed,
    EdgeInsets? itemTextPadding,
    EdgeInsets? selectedItemTextPadding,
    Color? hoverColor,
    TextStyle? hoverTextStyle,
    IconThemeData? hoverIconTheme,
  }) {
    return SidebarXTheme(
      width: width ?? this.width,
      height: height ?? this.height,
      padding: padding ?? this.padding,
      margin: margin ?? this.margin,
      decoration: decoration ?? this.decoration,
      iconTheme: iconTheme ?? this.iconTheme,
      selectedIconTheme: selectedIconTheme ?? this.selectedIconTheme,
      textStyle: textStyle ?? this.textStyle,
      selectedTextStyle: selectedTextStyle ?? this.selectedTextStyle,
      itemDecoration: itemDecoration ?? this.itemDecoration,
      selectedItemDecoration:
          selectedItemDecoration ?? this.selectedItemDecoration,
      itemMargin: itemMargin ?? this.itemMargin,
      selectedItemMargin: selectedItemMargin ?? this.selectedItemMargin,
      itemPadding: itemPadding ?? this.itemPadding,
      itemPaddingCollapsed: itemPaddingCollapsed ?? this.itemPaddingCollapsed,
      selectedItemPadding: selectedItemPadding ?? this.selectedItemPadding,
      selectedItemPaddingCollapsed:
          selectedItemPaddingCollapsed ?? this.selectedItemPaddingCollapsed,
      itemTextPadding: itemTextPadding ?? this.itemTextPadding,
      selectedItemTextPadding:
          selectedItemTextPadding ?? this.selectedItemTextPadding,
      hoverColor: hoverColor ?? this.hoverColor,
      hoverTextStyle: hoverTextStyle ?? this.hoverTextStyle,
      hoverIconTheme: hoverIconTheme ?? this.hoverIconTheme,
    );
  }
}
